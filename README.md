# Installation
### Voraussetzung
* nodejs

### Ablauf
* `npm install` in beiden Modulen ausführen
* `ng serve` in Angular-Projekt starten
* `ionic serve` in Ionic-Projekt starten
* Angular-Projekt auf `localhost:4200`, Ionic-Projekt auf `localhost:8100` bewundern
