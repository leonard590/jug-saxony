import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'pokemon-list', pathMatch: 'full' },
  { path: 'pokemon-list', loadChildren: './pokemon-list/pokemon-list.module#PokemonListPageModule' },
  { path: 'pokemon-form', loadChildren: './pokemon-form/pokemon-form.module#PokemonFormPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
