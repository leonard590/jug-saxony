import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../pokemon.service';
import {Pokemon} from '../models/pokemon.model';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.page.html',
  styleUrls: ['./pokemon-form.page.scss'],
})
export class PokemonFormPage implements OnInit {

  pokemon: Pokemon = {
    pokedex: undefined,
    name: '',
    type: [],
    pictureUrl: ''
  };

  constructor(
      private pokemonService: PokemonService,
      private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.pokemonService.addPokemon(this.pokemon);
    this.navCtrl.back();
  }

}
