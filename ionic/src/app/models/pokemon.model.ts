export interface Pokemon {
  pokedex: number;
  name: string;
  type: string[];
  pictureUrl: string;
}
