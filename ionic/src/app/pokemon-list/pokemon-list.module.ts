import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {PokemonListPage} from './pokemon-list.page';
import {PokemonListItemComponent} from './pokemon-list-item/pokemon-list-item.component';

const routes: Routes = [
    {
        path: '',
        component: PokemonListPage
    },
    {path: ':pokedex', loadChildren: '../pokemon-detail/pokemon-detail.module#PokemonDetailPageModule'}
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PokemonListPage, PokemonListItemComponent]
})
export class PokemonListPageModule {
}
