import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../models/pokemon.model';
import {PokemonService} from '../pokemon.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.page.html',
  styleUrls: ['./pokemon-detail.page.scss'],
})
export class PokemonDetailPage implements OnInit {

  pokemon: Pokemon;

  constructor(
      private pokemonService: PokemonService,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pokemon = this.pokemonService.getPokemon(+this.route.snapshot.paramMap.get('pokedex'));
  }

}
