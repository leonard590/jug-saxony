import {Injectable} from '@angular/core';
import {Pokemon} from './models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private pokemonList: Pokemon[] = [
    {
      pokedex: 1,
      name: 'Bisasam',
      type: ['Pflanze', 'Gift'],
      pictureUrl: 'https://www.pokewiki.de/images/9/96/Sugimori_001.png'
    },
    {
      pokedex: 4,
      name: 'Glumanda',
      type: ['Feuer'],
      pictureUrl: 'https://www.pokewiki.de/images/6/63/Sugimori_004.png'
    },
    {
      pokedex: 7,
      name: 'Schiggy',
      type: ['Wasser'],
      pictureUrl: 'https://www.pokewiki.de/images/a/ac/Sugimori_007.png'
    }
  ];

  getPokemonList(): Pokemon[] {
    return this.pokemonList;
  }

  getPokemon(pokedex: number): Pokemon {
    return this.pokemonList.find((pokemon) => {
      return pokemon.pokedex === pokedex;
    });
  }

  addPokemon(pokemon: Pokemon) {
    this.pokemonList.push(pokemon);
  }
}
