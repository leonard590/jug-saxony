import {Component, OnInit} from '@angular/core';
import {PokemonService} from '../pokemon.service';
import {Pokemon} from '../models/pokemon.model';

@Component({
  selector: 'jug-pokemon-list',
  templateUrl: './pokemon-list.component.html'
})
export class PokemonListComponent implements OnInit {

  pokemonList: Pokemon[];

  constructor(
    private pokemonService: PokemonService
  ) {}

  ngOnInit() {
    this.pokemonList = this.pokemonService.getPokemonList();
  }

  pokemonClicked(index: number) {
    console.log(this.pokemonList[index].name + ' was clicked');
  }
}
