import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../models/pokemon.model';
import {PokemonService} from '../pokemon.service';
import {Router} from '@angular/router';

@Component({
  selector: 'jug-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.css']
})
export class PokemonFormComponent implements OnInit {

  pokemon: Pokemon = {
    pokedex: undefined,
    name: '',
    type: [],
    pictureUrl: ''
  };

  constructor(
    private pokemonService: PokemonService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.pokemonService.addPokemon(this.pokemon);
    this.router.navigate(['/pokemon']);
  }
}
