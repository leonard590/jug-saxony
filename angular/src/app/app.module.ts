import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PokemonListComponent} from './pokemon-list/pokemon-list.component';
import {Route, RouterModule} from '@angular/router';
import {PokemonDetailComponent} from './pokemon-detail/pokemon-detail.component';
import {PokemonFormComponent} from './pokemon-form/pokemon-form.component';
import {FormsModule} from '@angular/forms';

const routes: Route[] = [
  {
    path: 'pokemon',
    component: PokemonListComponent
  },
  {
    path: 'pokemon/form',
    component: PokemonFormComponent
  },
  {
    path: 'pokemon/:pokedex',
    component: PokemonDetailComponent
  },
  {
    path: '',
    redirectTo: 'pokemon',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonDetailComponent,
    PokemonFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
