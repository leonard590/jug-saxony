import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../pokemon.service';
import {Pokemon} from '../models/pokemon.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'jug-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {
  pokemon: Pokemon;

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pokemon = this.pokemonService.getPokemon(+this.route.snapshot.paramMap.get('pokedex'));
  }

}
